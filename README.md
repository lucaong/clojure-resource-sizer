# resource-sizer

A Clojure microservice to calculate the byte size of one or more resources

## Usage

Start the service, say on port 7777:

`lein run 7777`

Now post on the root passing one or more URLs to resources:

`curl -d 'urls[]=http://somesite.com/image.jpg&urls[]=http://somesite.com/missing_image.jpg' http://localhost:7777`

The response will be a JSON object mapping given URLs to HTTP status and byte
length (byte length only given when the status is 200):

```
{
  "http://somesite.com/image.jpg": {
    "status": 200,
    "bytes": 224902
  },
  "http://somesite.com/missing_image.jpg": {
    "status": 404
  }
}
```

The byte size will be inferred from the "Content-Length" header. Redirects are
followed.

## License

Copyright © 2014 Luca Ongaro
