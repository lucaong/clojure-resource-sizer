(ns resource-sizer.core
  (:gen-class)
  (:use [compojure.core :only [defroutes GET POST]]
        org.httpkit.server)
  (:require [org.httpkit.client :as http]
            [clojure.data.json  :as json]
            [ring.middleware.defaults :refer :all]))

(defn- get-content-length [res]
  (Integer. (get-in res [:headers :content-length] "0")))

(defn calculate-sizes
  [urls]
  (->> urls
       (map (fn [url] (http/head url {:headers {"Accept-Encoding" ""}})))
       (map (fn [res] 
              (let [status (:status @res)]
                (if (= status 200)
                  {:status status :bytes (get-content-length @res)}
                  {:status status} ))))))

(defroutes app
  (POST "/" [urls]
       {:headers {"Content-Type" "application/json"}
        :body    (json/write-str
                   (->> urls (calculate-sizes) (interleave urls) (apply assoc {}))) })
  (GET "/ping" [] "pong"))

(defn -main
  [& args]
  (let [port (or (first args) 7005)]
    (run-server
      (wrap-defaults app (select-keys site-defaults [:params]))
      {:port (Integer. port)})
    (println "Server started on port" port) ))
