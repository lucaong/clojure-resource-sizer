(ns resource-sizer.core-test
  (:use [compojure.core :only [defroutes ANY]]
        org.httpkit.server)
  (:require [clojure.test :refer :all]
            [resource-sizer.core :refer :all]))

(defroutes test-routes
  (ANY "/image1.jpg" [] {:status 200 :body "this is 21 bytes long"})
  (ANY "/image2.jpg" [] {:status 200 :body "and this is 25 bytes long"})
  (ANY "/image3.jpg" [] {:status 302 :headers {"Location" "/image4.jpg"}})
  (ANY "/image4.jpg" [] {:status 200 :body "and this is instead 33 bytes long"})
  (ANY "/missing_image.jpg" [] {:status 404}))

(use-fixtures :once
              (fn [tests]
                (let [server (run-server test-routes {:port 9875})]
                  (try (tests) (finally (server))) )))

(deftest test-calculate-sizes
  (let [urls ["http://localhost:9875/image1.jpg"
              "http://localhost:9875/image2.jpg"
              "http://localhost:9875/image3.jpg"
              "http://localhost:9875/missing_image.jpg"]
        result (apply assoc {} (interleave urls (calculate-sizes urls)))]

    (testing "it retrieves statuses and length for all resources"
      (is (= (get-in result ["http://localhost:9875/image1.jpg" :status]) 200))
      (is (= (get-in result ["http://localhost:9875/image1.jpg" :bytes]) 21))
      (is (= (get-in result ["http://localhost:9875/image2.jpg" :status]) 200))
      (is (= (get-in result ["http://localhost:9875/image2.jpg" :bytes]) 25)))

    (testing "it follows redirects"
      (is (= (get-in result ["http://localhost:9875/image3.jpg" :status]) 200))
      (is (= (get-in result ["http://localhost:9875/image3.jpg" :bytes]) 33)))

    (testing "it does not blow up when status is not 200"
      (is (= (get-in result ["http://localhost:9875/missing_image.jpg" :status]) 404)) )))
