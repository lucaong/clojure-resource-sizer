(defproject resource-sizer "0.1.0-SNAPSHOT"
  :description "Calculate size in bytes of given resources"
  :url "http://example.com/FIXME"
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [http-kit "2.1.18"]
                 [compojure "1.2.1"]
                 [ring "1.2.1"]
                 [org.clojure/data.json "0.2.5"]
                 [ring/ring-defaults "0.1.2"]]
  :ubejar-name "resource-sizer"
  :profiles {:uberjar {:main resource-sizer.core :aot :all}}
  :main resource-sizer.core)
